<?php
// $Id$

/**
 * @file
 * Pages of Boobaa's 2nd homework for #tuksi02.
 */

/**
 * Lists the available boobaa_2 phone book entries.
 */
function boobaa_2_page() {
  $pagelen = 5;
  $filter = boobaa_2_page_build_filter_query();
  $output = drupal_get_form('boobaa_2_form_filter');
  $header = array(
    array('data' => t('Phone number'), 'field' => 'phone_number'),
    array('data' => t('Name'), 'field' => 'name'),
  );
  $can_edit = user_access('edit boobaa_2 phone book entry');
  $can_delete = user_access('delete boobaa_2 phone book entry');
  if ($can_edit || $can_delete) {
    $header[] = array('data' => t('Operations'), 'colspan' => '2');
  }
  $sql = 'SELECT pbid, phone_number, name FROM {boobaa_2}';
  $tablesort = tablesort_sql($header);
  $result = pager_query($sql . $tablesort, $pagelen);
  if(!empty($filter['where'])) {
    $result = pager_query($sql .' WHERE '. $filter['where'] . $tablesort, $pagelen, 0, NULL, $filter['args']);
  }
  else {
    $result = pager_query($sql . $tablesort, $pagelen);
  }
  $rows = array();
  while ($entry = db_fetch_object($result)) {
    $row = array(
      check_plain($entry->phone_number),
      check_plain($entry->name),
    );
    if ($can_edit) {
      $row[] = l(t('Edit'), 'boobaa_2/'. $entry->pbid .'/edit');
    }
    if ($can_delete) {
      $row[] = l(t('Delete'), 'boobaa_2/'. $entry->pbid .'/delete');
    }
    $rows[] = $row;
  }
  if (empty($rows)) {
    $output .= t('There is no phone book entry for boobaa_2 (that meets the filter).');
  }
  else {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, $pagelen, 0);
  }
  if (user_access('create boobaa_2 phone book entry')) {
    $output .= drupal_get_form('boobaa_2_form_add', 'list');
  }
  return $output;
}

/**
 * Adds a non-existing boobaa_2 phone book entry.
 *
 * @ingroup forms
 * @see boobaa_2_form_add_validate()
 * @see boobaa_2_form_add_submit()
 */
function boobaa_2_form_add($form_state, $phase = 'add') {
  $form['pbid'] = array(
    '#type' => 'value',
    '#value' => -1, // -1 stands for 'new'.
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 250,
    '#required' => TRUE,
  );
  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#size' => 30,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('Must be unique.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // To have the same validator both for add and edit forms.
  if (!isset($form['#validate'])) {
    $form['#validate'] = array();
  }
  $form['#validate'][] = 'boobaa_2_form_add_validate';
  switch ($phase) {
  case 'list':
    $form['#prefix'] = theme('boobaa_2_form_prefix', t('Add a boobaa_2 phone book entry'));
    break;
  case 'edit':
    // The edit builder sets the title by itself.
    break;
  case 'add':
  default:
    drupal_set_title(t('Add a boobaa_2 phone book entry'));
  }
  return $form;
}

/**
 * Verify that a (new) boobaa_2 phone book entry is valid.
 *
 * @see boobaa_2_form_add()
 * @see boobaa_2_form_edit()
 * @see boobaa_2_form_add_submit()
 */
function boobaa_2_form_add_validate($form, &$form_state) {
  $old_pbid = db_result(db_query("SELECT pbid FROM {boobaa_2} WHERE phone_number = '%s' AND (NOT (pbid = %d))", $form_state['values']['phone_number'], $form_state['values']['pbid']));
  if ($old_pbid) {
    form_set_error('phone_number', t('This phone number already exists.'));
  }
}

/**
 * Save a new boobaa_2 phone book entry to the database.
 *
 * @see boobaa_2_form_add()
 * @see boobaa_2_form_add_validate()
 */
function boobaa_2_form_add_submit($form, &$form_state) {
  unset($form_state['values']['pbid']);
  $saved = drupal_write_record('boobaa_2', $form_state['values']);
  if ($saved) {
    watchdog('boobaa_2', '%name has been added to the phone book.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'boobaa_2/'. $form_state['values']['pbid'] . '/edit'));
    drupal_set_message(t('%name has been added to the phone book.', array('%name' => $form_state['values']['name'])));
  }
  else {
    drupal_set_message(t('%name could not be saved to the phone book (phone number is not unique).', array('%name' => $form_state['values']['name'])), 'warning');
  }
  $form_state['redirect'] = 'boobaa_2';
}

/**
 * Prepares an edit form for a boobaa_2 phone book entry.
 *
 * @ingroup forms
 * @see boobaa_2_form_edit_submit()
 */
function boobaa_2_form_edit($form_state, $entry) {
  $form = boobaa_2_form_add($form, 'edit');
  $form['pbid']['#value'] = $entry->pbid;
  $form['phone_number']['#default_value'] = $entry->phone_number;
  $form['name']['#default_value'] = $entry->name;
  drupal_set_title(check_plain($entry->name));
  return $form;
}

/**
 * Saves a modified boobaa_2 phone book entry to the database.
 *
 * @see boobaa_2_form_edit()
 */
function boobaa_2_form_edit_submit($form, &$form_state) {
  $saved = drupal_write_record('boobaa_2', $form_state['values'], 'pbid');
  if ($saved) {
    watchdog('boobaa_2', 'Phone book entry of %name has been updated.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'boobaa_2/'. $form_state['values']['pbid'] . '/edit'));
    drupal_set_message(t('Phone book entry of %name has been updated.', array('%name' => $form_state['values']['name'])));
  }
  else {
    drupal_set_message(t('%name could not be saved to the phone book (phone number is not unique).', array('%name' => $form_state['values']['name'])), 'warning');
  }
  $form_state['redirect'] = 'boobaa_2';
}

/**
 * Confirms deleting a boobaa_2 phone book entry.
 *
 * @see boobaa_2_form_delete_submit()
 */
function boobaa_2_form_delete($form_state, $entry) {
  $form['pbid'] = array(
    '#type' => 'value',
    '#value' => $entry->pbid,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $entry->name,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to delete the phone number of %name?', array('%name' => $entry->name)),
    'boobaa_2',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute boobaa_2 phone book entry deletion.
 *
 * @see boobaa_2_form_delete()
 */
function boobaa_2_form_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {boobaa_2} WHERE pbid = %d", $form_state['values']['pbid']);
  watchdog('boobaa_2', 'Phone book entry of %name has been deleted.', array('%name' => $form_state['values']['name']));
  drupal_set_message(t('Phone book entry of %name has been deleted.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'boobaa_2';
}

/**
 * Default theme function for boobaa_2_form_prefix().
 */
function theme_boobaa_2_form_prefix($prefix = NULL) {
  return '<h2>'. $prefix .'</h2>';
}

/**
 * Adds filter capability to boobaa_2_page().
 *
 * @ingroup forms
 * @see boobaa_2_form_filter_validate
 * @see boobaa_2_form_filter_submit
 */
function boobaa_2_form_filter() {
  $session = &$_SESSION['boobaa_2_page_filter'];
  $session = is_array($session) ? $session : array();
  $filters = boobaa_2_page_filters();
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter boobaa_2 phone book'),
    '#theme' => 'boobaa_2_form_filter',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => $filter['type'],
    );
    foreach(array('options', 'maxlength', 'size', 'autocomplete_path', 'description') as $arg) {
      if(isset($filter[$arg])) {
        $form['filters']['status'][$key]["#$arg"] = $filter[$arg];
      }
    }
    if(!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }
  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if(!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }
  return $form;
}

/**
 * Validates filtering form for boobaa_2_page().
 */
function boobaa_2_form_filter_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['name']) && empty($form_state['values']['phone_number'])) {
    form_set_error('name', t('You must select something to filter by.'));
  }
}

/**
 * Stores the filtering options for boobaa_2_page() into session variables.
 */
function boobaa_2_form_filter_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = boobaa_2_page_filters();
  switch($op) {
  case t('Filter'):
    foreach($filters as $name => $filter) {
      if (isset($form_state['values'][$name])) {
        $_SESSION['boobaa_2_page_filter'][$name] = $form_state['values'][$name];
      }
    }
    break;
  case t('Reset'):
    $_SESSION['boobaa_2_page_filter'] = array();
    break;
  }
  // Redirect to the list.
  $form_state['redirect'] = 'boobaa_2';
}

/**
 * Returns the available filtering options for both boobaa_2_page() and
 * boobaa_2_page_build_filter_query().
 */
function boobaa_2_page_filters() {
  $filters = array();
  $filters['name'] = array(
    'type' => 'textfield',
    'title' => t('Name'),
    'where' => "name LIKE '%%%s%%'",
    'description' => t('Name should contain this.'),
    'maxlength' => 100,
  );
  $filters['phone_number'] = array(
    'type' => 'textfield',
    'title' => t('Phone number'),
    'where' => "phone_number LIKE '%%%s'",
    'maxlength' => 30,
    'description' => t('Phone number should end with this.'),
    'size' => 30,
  );
  return $filters;
}

/**
 * Returns a pair of filter/args array for boobaa_2_page(), filled with
 * values based on session variables, if any.
 */
function boobaa_2_page_build_filter_query() {
  if (empty($_SESSION['boobaa_2_page_filter'])) {
    return;
  }
  $filters = boobaa_2_page_filters();
  $where = $args = array();
  foreach ($_SESSION['boobaa_2_page_filter'] as $key => $filter) {
    $filter_where = array();
    if (is_array($filter)) {
      foreach($filter as $value) {
        if(!empty($value)) {
          $filter_where[] = $filters[$key]['where'];
          $args[] = $value;
        }
      }
    }
    elseif (!empty($filter)) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $filter;
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';
  return array(
    'where' => $where,
    'args' => $args,
  );
}

/**
 * Default theme function for boobaa_2_form_filter().
 */
function theme_boobaa_2_form_filter($form = NULL) {
  drupal_add_css(drupal_get_path('module', 'boobaa_2') .'/boobaa_2.css');
  $buttons = drupal_render($form['buttons']);
  return drupal_render($form) .'<div class="boobaa-2-page-filter-buttons">'. $buttons .'</div>';
}

