<?php
// $Id


/* hook_views_default_views(): alapértelmezett nézet */
function nevergone_2_views_views_default_views() {
  /* kiexportált nézet */
  $view = new view;
  $view->name = 'nevergone_2_views';
  $view->description = 'nevergone_2 telefonkönyv Views segítségével';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'nevergone_2';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Alapértelmezések', 'default');
  $handler->override_option('fields', array(
    'id' => array(
      'label' => 'Azonosító',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'id',
      'table' => 'nevergone_2',
      'field' => 'id',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Név',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'name',
      'table' => 'nevergone_2',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'phone' => array(
      'label' => 'Telefonszám',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'phone',
      'table' => 'nevergone_2',
      'field' => 'phone',
      'relationship' => 'none',
    ),
    'edit_link' => array(
      'label' => 'Szerkesztés',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'edit_link',
      'table' => 'nevergone_2',
      'field' => 'edit_link',
      'relationship' => 'none',
    ),
    'delete_link' => array(
      'label' => 'Törlés',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'delete_link',
      'table' => 'nevergone_2',
      'field' => 'delete_link',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'id' => array(
      'order' => 'ASC',
      'id' => 'id',
      'table' => 'nevergone_2',
      'field' => 'id',
      'override' => array(
        'button' => 'Felülírás',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'elem megtekintése',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'id' => 'id',
      'name' => 'name',
      'phone' => 'phone',
      'edit_link' => 'edit_link',
      'delete_link' => 'delete_link',
    ),
    'info' => array(
      'id' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'phone' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'edit_link' => array(
        'separator' => '',
      ),
      'delete_link' => array(
        'separator' => '',
      ),
    ),
    'default' => 'id',
  ));
  $handler = $view->new_display('page', 'Telefonkönyv táblázat', 'page_1');
  $handler->override_option('title', 'Telefonkönyv táblázat');
  $handler->override_option('path', 'nevergone_2_views');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('block', 'Legutolsó bejegyzések', 'block_1');
  $handler->override_option('fields', array(
    'id' => array(
      'label' => 'Azonosító',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'id',
      'table' => 'nevergone_2',
      'field' => 'id',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Név',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'name',
      'table' => 'nevergone_2',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'phone' => array(
      'label' => 'Telefonszám',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'phone',
      'table' => 'nevergone_2',
      'field' => 'phone',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'id' => array(
      'order' => 'DESC',
      'id' => 'id',
      'table' => 'nevergone_2',
      'field' => 'id',
      'override' => array(
        'button' => 'Alapértelmezés használata',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Legutolsó bejegyzések');
  $handler->override_option('items_per_page', 5);
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array());
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  // exportált nézet vége
  $views[$view->name] = $view;
  return $views;
}
