<?php
// $Id


/* handler a szerkesztés linkhez, a tábla 'id' mezője alapján elkészíti a szerkesztés linket */
class nevergone_2_views_edit_handler extends views_handler_field_markup {
  function render($values) {
    /* kinyerjük a mező értékét a lekérdezésben, hogy át tudjuk alakítani. */
    $value = $values->{$this->field_alias};
    /* a végeredmény egy link lesz adott szöveggel */
    $value = l('Szerkesztés', 'nevergone_2/' . $value . '/edit');
    return $value;
  }
}
