<?php
// $Id


/* handler a törlés linkhez, a tábla 'id' mezője alapján elkészíti a törlés linket */
class nevergone_2_views_delete_handler extends views_handler_field_markup {
  function render($values) {
    /* kinyerjük a mező értékét a lekérdezésben, hogy át tudjuk alakítani. */
    $value = $values->{$this->field_alias};
    /* a végeredmény egy link lesz a törléshez */
    $value = l('Törlés', 'nevergone_2/' . $value . '/delete/' . drupal_get_token('nevergone_2'));
    return $value;
  }
}
