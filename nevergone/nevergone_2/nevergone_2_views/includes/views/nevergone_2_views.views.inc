<?php
// $Id


/* hook_views_data(): nevergone_2 adattábla kezelése Views-ben */
function nevergone_2_views_views_data() {
  /* nézethez szükséges alapvető adatok */
  $data['nevergone_2']['table']['group'] = 'nevergone_2';
  $data['nevergone_2']['table']['base'] = array(
    'field' => 'id',
    'title' => '#tuksi02 telefonkönyv (nevergone_2)',
  );
  /* nevergone_2 tábla 'id' mezője */
  $data['nevergone_2']['id'] = array(
    'title' => 'Azonosító',
    'help' => 'Bejegyzés azonosítója.',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  /* nevergone_2 tábla 'name' mezője */
  $data['nevergone_2']['name'] = array(
    'title' => 'Név',
    'help' => 'Bejegyzésben tárolt név.',
    /* mezőként megjelenik */
    'field' => array(
      'handler' => 'views_handler_field_markup',
      'format' => FILTER_FORMAT_DEFAULT,
      'click sortable' => TRUE,
    ),
    /* argumentumként használható */
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    /* szűrhető */
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    /* rendezhető */
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // nevergone_2 tábla 'phone' mezője
  $data['nevergone_2']['phone'] = array(
    'title' => 'Telefonszám',
    'help' => 'Bejegyzésben tárolt telefonszám.',
    'field' => array(
      'handler' => 'views_handler_field_markup',
      'format' => FILTER_FORMAT_DEFAULT,
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  /* szerkesztés link */
  $data['nevergone_2']['edit_link'] = array(
    'real field' => 'id', /* mivel ez nem valós mező az adattáblában, megadjuk, hogy melyiken alapul */
    'field' => array(
      'title' => 'Szerkesztés',
      'help' => 'Bejegyzés szerkesztése.',
      'handler' => 'nevergone_2_views_edit_handler',
      'format' => FILTER_FORMAT_DEFAULT,
    ),
  );
  /* törlés link */
  $data['nevergone_2']['delete_link'] = array(
    'real field' => 'id',
    'field' => array(
      'title' => 'Törlés',
      'help' => 'Bejegyzés törlése.',
      'handler' => 'nevergone_2_views_delete_handler',
      'format' => FILTER_FORMAT_DEFAULT,
    ),
  );
  return $data;
}


/* hook_views_handlers(): saját mező-kezelők regisztrálása */
function nevergone_2_views_views_handlers() {
  return array(
    /* könyvtár helye, amely a mező-kezelőket tartalmazza .inc kiterjesztéssel */
    'info' => array(
      'path' => drupal_get_path('module', 'nevergone_2_views') . '/includes/views/handlers',
    ),
    /* mező-kezelők regisztrálása */
    'handlers' => array(
      'nevergone_2_views_edit_handler' => array(
        /* melyik mezőkezelőből származtatjuk */
        'parent' => 'views_handler_field_markup',
      ),
      'nevergone_2_views_delete_handler' => array(
        'parent' => 'views_handler_field_markup',
      ),
    ),
  );
}
