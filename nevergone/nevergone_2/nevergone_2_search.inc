<?php
// $Id$


/* keresés-form */
function nevergone_2_search_form($form_state, $data = NULL) {
  /* keresés */
  $form['search_field'] = array(
    '#type' => 'fieldset',
    '#title' => 'Keresés',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['search_field']['search_name'] = array( /* név */
    '#type' => 'textfield',
    '#title' => 'Név',
    '#autocomplete_path' => 'nevergone_2/search_autocomplete/name',
  );
  $form['search_field']['search_phone'] = array( /* telefonszám */
    '#type' => 'textfield',
    '#title' => 'Telefonszám',
    '#autocomplete_path' => 'nevergone_2/search_autocomplete/phone',
  );
  $form['search_field']['search_submit'] = array( /* "submit" gomb */
    '#type' => 'submit',
    '#value' => 'Keresés',
  );
  return $form;
}


/* keresésnél automatikus kiegészítés a név mezőre */
function nevergone_2_search_name($name = '') {
  $matches = array();
  if ($name) {
    $result = db_query_range("SELECT name FROM {nevergone_2} WHERE LOWER(name) LIKE LOWER('%s%%')", $name, 0, 10);
    while ($row = db_fetch_array($result)) {
      $matches[$row['name']] = check_plain($row['name']);
    }
  }
  drupal_json($matches);
}


/* keresésnél automatikus kiegészítés a telefonszám mezőre */
function nevergone_2_search_phone($phone = '') {
  $matches = array();
  if ($phone) {
    $result = db_query_range("SELECT phone FROM {nevergone_2} WHERE phone LIKE '%s%%'", $phone, 0, 10);
    while ($row = db_fetch_array($result)) {
      $matches[$row['phone']] = check_plain($row['phone']);
    }
  }
  drupal_json($matches);
}


/* kereső-form megjelenítése */
function nevergone_2_search($s_name = '', $s_phone = '') {
  $output = drupal_get_form('nevergone_2_search_form');
  /* fejléc készítése a megjelenített táblázathoz */
  $header = array(
    array(
      'field' => 'id',
      'data' => 'Azonosító',
    ),
    array(
      'field' => 'name',
      'data' => 'Név',
    ),
    array(
      'field' => 'id',
      'data' => 'Telefonszám',
    ),
    array(
      'data' => 'Szerkesztés / Törlés',
    ),
  );
  /* van paraméter, keresés indul */
  if (!empty($s_name) || !empty($s_phone)) {
    $s_name = check_plain($s_name);
    $s_phone = check_plain($s_phone);
    /* SQL lekérdezéshez a WHERE feltétel összeállítása */
    $where_name = "name LIKE '%%%s%%'";
    $where_phone = "phone LIKE '%%%s%%'";
    if ($s_name && $s_phone) {
      $where = $where_name . ' AND ' . $where_phone;
      $where_value[] = $s_name;
      $where_value[] = $s_phone;
    }
    else {
      $where = ($s_name) ? $where_name : $where_phone;
      $where_value[] = ($s_name) ? $s_name : $s_phone;
    }
    /* elemek listája rendezhetően */
    $res = pager_query("SELECT * FROM {nevergone_2} WHERE " . $where . tablesort_sql($header), 10, 0, NULL, $where_value);
    while ($row = db_fetch_array($res)) {
      /* adatbázisból kiolvasott elemeket tömbbe pakoljuk a táblázatos megjelenítéshez */
      $id = check_plain($row['id']);
      $tr = array(); /* egy sor elkészítése */
      $tr[] = $id;
      $tr[] = check_plain($row['name']);
      $tr[] = check_plain($row['phone']);
      $tr[] = l('szerkesztés', 'nevergone_2/' . $id . '/edit') . ' / ' . l('törlés', 'nevergone_2/' . $id . '/delete');
      $table[] = $tr; /* sor a táblázatba */
    }
  }
  $output .= theme('table', $header, $table);
  $output .= theme('pager');
  return $output;
}


/* keresés formon a submit gombra kattintottak */
function nevergone_2_search_form_submit($form, &$form_state) {
  $s_name = $form_state['values']['search_name'];
  $s_phone = $form_state['values']['search_phone'];
  $form_state['redirect'] = 'nevergone_2/search/' . $s_name . '/' . $s_phone;
}
