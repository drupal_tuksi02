<?php
// $Id$

/**
 * @file
 * Theme functions to theme colourized node title.
 */


/**
 * Theme function to wrap the title of a given node into spans with class according to preferences.
 */
function theme_colourized_title($node) {
  if ($node->colour != 'none') {
    $attr = array('style' => 'color: ' . $node->colour);
    // Using drupal_attrbutes so we don't have to do additional isset($node->colour) testing and we'll
    // end up with valid HTML.
    return '<span' . drupal_attributes($attr) . '">' . check_plain($node->title) . '</span>';
  }
  else {
   return check_plain($node->title);
  }
}