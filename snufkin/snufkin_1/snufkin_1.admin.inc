<?php
// $Id$

/**
 * @file
 *
 * Administration related functions and callbacks.
 */
 
/**
 * Configuration form for the embed widget.
 */
function snufkin_1_settings_form() {
  $form = array();
  $form['snufkin_1_name'] = array(
    '#title' => t('Account name'),
    '#type' => 'textfield',
    '#description' => t('Name of the delicious.com account to use for the widget'),
    '#default_value' => variable_get('snufkin_1_name', 'tanarurkerem'),
  );
  return system_settings_form($form);
}
